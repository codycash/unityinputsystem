using System.Collections.Generic;
using CustomInputSystem;
using UnityEngine;

public class UnityInputManager : InputManager {

    [SerializeField]
    private string playerAxisPrefix = "";

    [SerializeField]
    private int maxNumberOfPlayers = 1;

    [SerializeField] private string jumpAxis = "Jump";
    [SerializeField] private string attackAxis = "Fire1";
    [SerializeField] private string subAttackAxis = "Fire2";
    [SerializeField] private string dashAxis = "Fire3";
    [SerializeField] private string runAxis = "Run";
    [SerializeField] private string moveHorizontalAxis = "Horizontal";
    [SerializeField] private string moveVerticalAxis = "Vertical";
    [SerializeField] private string pauseAxis = "Cancel";

    private Dictionary<int, string>[] actions;

    protected override void Awake () {
        base.Awake ();
        if (InputManager.Instance != null) {
            isEnabled = false;
            return;
        }
        SetInstance (this);

        actions = new Dictionary<int, string>[maxNumberOfPlayers];

        for (int i = 0; i < maxNumberOfPlayers; i++) {
            var playerActions = new Dictionary<int, string> ();
            actions[i] = playerActions;
            string prefix = !string.IsNullOrEmpty (playerAxisPrefix) ? playerAxisPrefix + i : string.Empty;

            AddAction (InputAction.Jump, prefix + jumpAxis, playerActions);
            AddAction (InputAction.Attack, prefix + attackAxis, playerActions);
            AddAction (InputAction.SubAttack, prefix + subAttackAxis, playerActions);
            AddAction (InputAction.Dash, prefix + dashAxis, playerActions);
            AddAction (InputAction.Run, prefix + runAxis, playerActions);
            AddAction (InputAction.MoveHorizontal, prefix + moveHorizontalAxis, playerActions);
            AddAction (InputAction.MoveVertical, prefix + moveVerticalAxis, playerActions);
            AddAction (InputAction.Pause, prefix + pauseAxis, playerActions);
        }
    }

    private static void AddAction (InputAction action, string actionName, Dictionary<int, string> actions) {
        if (string.IsNullOrEmpty (actionName)) { return; }
        actions.Add ((int) action, actionName);
    }

    public override float GetAxis (int playerid, InputAction action) {
        return Input.GetAxisRaw (actions[playerid][(int) action]);
    }

    public override bool GetButton (int playerid, InputAction action) {
        return Input.GetButton (actions[playerid][(int) action]);
    }

    public override bool GetButtonDown (int playerid, InputAction action) {
        return Input.GetButtonDown (actions[playerid][(int) action]);
    }

    public override bool GetButtonUp (int playerid, InputAction action) {
        return Input.GetButtonUp (actions[playerid][(int) action]);
    }
}