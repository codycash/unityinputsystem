using CustomInputSystem;
using UnityEngine;

public abstract class InputManager : MonoBehaviour, IInputManager {
    private static InputManager instance;

    public static IInputManager Instance { get { return instance; } }

    public static void SetInstance (InputManager theInstance) {
        if (InputManager.instance == theInstance) { return; }
        if (InputManager.instance != null) {
            InputManager.instance.enabled = false;
        }

        InputManager.instance = theInstance;
    }

    private bool _dontDestroyOnLoad = true;

    protected virtual void Awake () {
        if (_dontDestroyOnLoad) { DontDestroyOnLoad (this.transform.root.gameObject); }
    }
    public virtual bool isEnabled {
        get {
            return this.isActiveAndEnabled;
        }
        set {
            this.enabled = value;
        }
    }

    public abstract bool GetButton (int playerid, InputAction action);

    public abstract bool GetButtonDown (int playerid, InputAction action);

    public abstract bool GetButtonUp (int playerid, InputAction action);

    public abstract float GetAxis (int playerid, InputAction action);
}