﻿namespace CustomInputSystem {
    public enum InputAction {
        None = 0,
        Jump = 1,
        Attack = 2,
        SubAttack = 3,
        Dash = 4,
        Run = 5,
        MoveHorizontal = 6,
        MoveVertical = 7,
        Pause = 8
    }
}