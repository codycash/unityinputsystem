namespace CustomInputSystem {
    public interface IInputManager {
        bool isEnabled { get; set; }
        bool GetButton (int playerid, InputAction action);
        bool GetButtonDown (int playerid, InputAction action);
        bool GetButtonUp (int playerid, InputAction action);
        float GetAxis (int playerid, InputAction action);
    }
}