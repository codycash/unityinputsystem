﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
public class InputTester : MonoBehaviour {
    public TextMeshProUGUI Joysticks;
    public TextMeshProUGUI[] InputText;
    public TextMeshProUGUI[] ButtonText;

    public int NumSticks;

    private void Start () {
        var sticks = "Joysticks\n";

        for (int i = 0; i < Input.GetJoystickNames ().Length; i++) {
            sticks += i + ": " + Input.GetJoystickNames () [i].ToString () + "\n";
        }
        Joysticks.text = sticks;

        NumSticks = Input.GetJoystickNames ().Length;
    }

    /*
     * Read all axis of joystick inputs and display them for configuration purposes
     * Requires the following input managers
     *      Joy[N] Axis 1-9
     *      Joy[N] Button 0-20
     */
    void Update () {
        for (int i = 1; i <= NumSticks; i++) {
            string inputs = "Joystick " + i + "\n";

            string stick = "Joy " + i + " Axis ";

            for (int a = 1; a <= 10; a++) {
                inputs += "Axis " + a + ":" + Input.GetAxis (stick + a).ToString ("0.00") + "\n";
            }

            InputText[i - 1].text = inputs;
        }

        string buttons = "Buttons 3\n";

        for (int b = 0; b <= 10; b++) {
            buttons += "Btn " + b + ":" + Input.GetButton ("Joy 3 Button " + b) + "\n";
        }

        ButtonText[2].text = buttons;
    }
}